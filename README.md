## Colorify

A minimalist color conversion tool

### Prerequisites

- [NodeJS](https://nodejs.org/en/) -  is a JavaScript runtime built on Chrome's V8 JavaScript engine. Node.js uses an event-driven, non-blocking I/O model that makes it lightweight and efficient.
Node.js' package ecosystem, npm, is the largest ecosystem of open source libraries in the world.
- [ReactJS](https://reactjs.org/) - A JavaScript library for building user interfaces

### Demo
- [Live Demo](https://jodeio.github.io/colorify)


### Installation

#### NodeJS

- Binaries, installers, and source tarballs are available at https://nodejs.org.

#### ReactJS

```bash
$ npm install --save react
```

### Usage

Run demo project

```
git clone https://github.com/jodeio/colorify.git
npm install
npm start
```

### Deployment

```bash
$ npm run deploy
```

More on:
[create-react-app deployment](https://github.com/facebook/create-react-app/blob/master/packages/react-scripts/template/README.md)

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Authors

[Joshua de Guzman](www.devjdg.com)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details


